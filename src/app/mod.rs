//! App
//!
//! The application’s main business logic. Platform-agnostic and sort-of-pure.

use crate::Fail;

/// The application’s main business logic. Platform-agnostic and sort-of-pure.
pub trait App {

    /// Xx.
    fn try_new() -> Result<Self, Fail> where Self: std::marker::Sized;

    /// @TODO remove this, it’s just a placeholder
    fn hello(&self) -> String { String::from("The `hello()` method needs doing!") }

}
