//! AppCli
//!
//! Wraps `App`, for simple non-interactive command line usage.

use crate::{App,Fail};

/// Wraps `App`, for simple non-interactive command line usage.
pub struct AppCli<T> {
    app: T,
}

impl <T> AppCli<T> where T: App {

    pub fn try_new(app: T) -> Result<Self, Fail> { Ok(Self { app }) }

    pub fn hello(&self) -> String { self.app.hello() }

}
