//! Fail::Error
//!
//! The basic requirements of custom Rust `Error` types are:
//! - May implement `Debug`
//! - Must implement `Display`
//! - Must implement `Error`

use super::Fail;

/// @TODO return proper description
impl std::error::Error for Fail {
    fn description(&self) -> &str {
        "@TODO return proper description"
    }
}
