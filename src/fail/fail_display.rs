//! Fail::Display
//!
//! The basic requirements of custom Rust `Error` types are:
//! - May implement `Debug`
//! - Must implement `Display`
//! - Must implement `Error`

use super::Fail;

/// `format!("{}", fail)` is more succinct. Good for keeping unit tests short.
/// @TODO replace `123` with a ‘user message’ mapped from `self.code`
impl std::fmt::Display for Fail {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:.1}{}: {:?}: {:?} {:?} Fail: {}",
            format!("{:?}", self.space), self.code, 123, self.space,
                self.severity, self.xray_message)
    }
}
