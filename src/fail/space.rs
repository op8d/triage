//! Space
//!
//! The general context or area for debugging, eg `"Space::Settings"`.

/// The general context or area for debugging, eg `"Space::Settings"`.
#[derive(Debug)]
pub enum Space {
    /// A problem with the `settings` argument, passed into `App::try_new()`.
    Settings,
}
