//! Fail::Debug
//!
//! The basic requirements of custom Rust `Error` types are:
//! - May implement `Debug`
//! - Must implement `Display`
//! - Must implement `Error`

use super::Fail;

/// `format!("{:?}", fail)` is more verbose. Good for development and debugging.
/// @TODO replace `123` with a ‘user message’ mapped from `self.code`
impl std::fmt::Debug for Fail {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:.1}{}: {:?}: {:?} {:?} Fail: {} {}: {}: {}",
            format!("{:?}", self.space), self.code, 123, self.space,
                self.severity, env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"),
                self.here, self.xray_message)
    }
}
