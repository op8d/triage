//! Severity
//!
//! How bad the error is, and what to do about it.

/// How bad the error is, and what to do about it.
#[derive(Debug)]
pub enum Severity {
    /// Pauses the app, but users just need to click ‘OK’ to resume.
    Alert,
    /// Pauses the app, but users just need to click ‘Cancel’ or ‘OK’ to resume.
    Confirm,
    /// Stops the app from running, after a graceful shut-down.
    Epic,
    /// Pauses the app until some valid value has been submitted by the user.
    Prompt,
    /// Automatically recovered from, without notifying the user in any way.
    Quiet,
    /// Many fails start life as `Tbd`, meaning the severity is yet to be
    /// determined. Later, something like `fail.to(Severity::Alert)` will run.
    Tbd,
    /// Automatically recovered from, and the user gets a notification.
    Warn,
}
