//! Example 1: Hello TRIAGE Core
//!
//! Apps with custom and default `hello()` implementations.

use triage::{App,AppCli,Fail};

/// An example of an App with custom `hello()` implementation.
pub struct MyCustomApp;
impl App for MyCustomApp {
    fn try_new() -> Result<Self, Fail> { Ok(Self {}) }
    fn hello(&self) -> String { String::from("Hello MyCustomApp!") }
}

/// An example of an App which just uses the trait’s default `hello()` method.
pub struct MyDefaultApp;
impl App for MyDefaultApp {
    fn try_new() -> Result<Self, Fail> { Ok(Self {}) }
}

fn main() {
    run().unwrap()
}

fn run() -> Result<(), Fail> {
    let my_custom_app = MyCustomApp::try_new()?;
    println!("my_custom_app.hello(): {:?}", my_custom_app.hello());
    let my_custom_app_cli = AppCli::try_new(my_custom_app)?;
    println!("my_custom_app_cli.hello(): {:?}", my_custom_app_cli.hello());

    let my_default_app = MyDefaultApp::try_new()?;
    println!("my_default_app.hello(): {:?}", my_default_app.hello());
    let my_default_app_cli = AppCli::try_new(my_default_app)?;
    println!("my_default_app_cli.hello(): {:?}", my_default_app_cli.hello());

    Ok(())
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn hello_triage_core_my_custom_app() {
        let my_custom_app = MyCustomApp::try_new().unwrap();
        assert_eq!(my_custom_app.hello(),
            String::from("Hello MyCustomApp!"));
        let my_custom_app_cli = AppCli::try_new(my_custom_app).unwrap();
        assert_eq!(my_custom_app_cli.hello(),
            String::from("Hello MyCustomApp!"));
    }

    #[test]
    fn hello_triage_core_my_default_app() {
        let my_default_app = MyDefaultApp::try_new().unwrap();
        assert_eq!(my_default_app.hello(),
            String::from("The `hello()` method needs doing!"));
        let my_default_app_cli = AppCli::try_new(my_default_app).unwrap();
        assert_eq!(my_default_app_cli.hello(),
            String::from("The `hello()` method needs doing!"));
    }
}
